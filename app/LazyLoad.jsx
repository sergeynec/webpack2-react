import React from 'react';

export default class LazyLoad extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			component: null,
			isLoading: true
		};
	}

	componentDidMount() {
		System.import(`./${this.props.component}`).then((component) => this.setState({
			component: component.default,
			isLoading: false
		}));
	}

	render() {
		if (this.state.isLoading) {
			return <h1>Loading...</h1>
		}

		return React.createElement(this.state.component, this.props, this.props.children);
	}
};

import '../index.html';
import React from 'react';
import ReactDOM from 'react-dom';
import LazyLoad from './LazyLoad';

ReactDOM.render(
	<div>
		<h1 className="test">Test</h1>
		<LazyLoad component="Root" className="test">
			lalala
		</LazyLoad>
	</div>,
	document.getElementById('application')
);

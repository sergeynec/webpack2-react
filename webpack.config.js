const path = require('path');

module.exports = {
	entry: ['babel-polyfill', './app/app.jsx'],

	devtool: 'source-map',

	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'bundle.js'
	},

	module: {
		rules: [
			{
				test: /\.html?/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'file-loader',
					query: {
						name: '[name].[ext]'
					}
				}
			},
			{
				test: /\.jsx?/,
				exclude: /(node_modules|bower_components)/,
				use: [
					{
						loader: 'babel-loader',
						query: {
							presets: [['es2015', {modules: false}], 'react']
						}
					}
				]
			}
		]
	},

	resolve: {
		extensions: ['.js', '.jsx'],
		modules: [
			path.resolve(__dirname, 'node_modules'),
			path.resolve(__dirname, 'app')
		]
	},

	stats: 'normal',

	devServer: {
		contentBase: path.join(__dirname, "dist"),
		compress: true,
		port: 9000
	}
};
